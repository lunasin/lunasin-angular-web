'use strict';

angular.module('myApp.viewDebt', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view-debt/:id', {
    templateUrl: 'view-debt/view-debt.html',
    controller: 'ViewDebtCtrl'
  });
}])

.controller('ViewDebtCtrl', ['$scope', '$firebaseObject', '$routeParams', function($scope, $firebaseObject, $routeParams) {

  var ref = new Firebase('https://lunasin-web.firebaseio.com/' + $routeParams.id);

  $scope.debtObject = $firebaseObject(ref);

  /*
  $scope.debtName = debtObject.debtName;
  $scope.debtAmount = debtObject.debtAmount;
  $scope.debtDescription = debtObject.debtDescription;
  $scope.debtCategory = debtObject.debtCategory;
  $scope.creditor = debtObject.creditor;

  console.log('view-fireRef', debtObject);*/

}]);

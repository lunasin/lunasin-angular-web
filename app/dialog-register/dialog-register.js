'use strict';

angular.module('myApp.register', ['ngRoute'])

/*.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider.when('/add-debt', {
    templateUrl: 'add-debt/add-debt.html',
    controller: 'AddDebtCtrl'
  });
  //$locationProvider.html5Mode(true);
}])*/


.controller('DialogRegisterCtrl', ['$scope', '$firebaseArray', '$mdDialog', 'Auth', function($scope, $firebaseArray, $mdDialog, Auth) {

  $scope.register = function() {

    if ($scope.password == $scope.passwordAgain) {
      Auth.$createUser({
        email: $scope.email,
        password: $scope.password
      }).then(function(userData) {
        console.log("User created with uid: " + userData.uid);
        $mdDialog.cancel();
      }).catch(function(error) {
        $scope.password = null;
        $scope.passwordAgain = null;
        $scope.errorInvalidEmail = true;
        $scope.errorDiffPass = null;
      });

    } else {
      $scope.password = null;
      $scope.passwordAgain = null;
      $scope.errorDiffPass = true;
      $scope.errorInvalidEmail = null;
    }
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

}]);

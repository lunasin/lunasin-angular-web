'use strict';

angular.module('myApp.firebase-credit-array', [])

.factory('CreditArray', ['$firebaseArray', function($firebaseArray) {
  return {
    setCurrentAuth: function(currentAuth) {
      var ref = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/credits');
      return $firebaseArray(ref);
    }
  };
}]);

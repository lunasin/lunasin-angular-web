'use strict';

angular.module('myApp.firebase-debt-array', [])

.factory('DebtArray', ['$firebaseArray', function($firebaseArray) {
  return {
    setCurrentAuth: function(currentAuth) {
      var ref = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/debts');
      return $firebaseArray(ref);
    }
  };
}]);

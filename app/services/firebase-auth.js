'use strict';

angular.module('myApp.firebase-auth', [])

.factory('Auth', ['$firebaseAuth', function($firebaseAuth) {
  var ref = new Firebase('https://lunasin-web.firebaseio.com');
  return $firebaseAuth(ref);
}]);

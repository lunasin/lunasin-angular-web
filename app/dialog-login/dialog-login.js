'use strict';

angular.module('myApp.login', ['ngRoute'])

.controller('DialogLoginCtrl', ['$scope', 'Auth', '$mdDialog', function($scope, Auth, $mdDialog) {

  $scope.loginFacebook = function() {
    $mdDialog.cancel();
    Auth.$authWithOAuthPopup('facebook', function(error, authData) {
      if (error) {
        console.log(error);
      } else {
        console.log(authData);
      }
    });
  };

  $scope.loginGithub = function() {
    $mdDialog.cancel();
    Auth.$authWithOAuthPopup('github', function(error, authData) {
      if (error) {
        console.log(error);
      } else {
        console.log(authData);
      }
    });
  };

  $scope.loginEmail = function() {
    Auth.$authWithPassword({
      email: $scope.email,
      password: $scope.password
    }).then(function(authData) {
      //console.log('logged in as: ', authData.uid);
      $mdDialog.cancel();
    }).catch(function(error) {
      //console.error('authentication failed: ', error);
      $scope.email = null;
      $scope.password = null;
      $scope.errorEmailLogin = true;
    });
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

}]);

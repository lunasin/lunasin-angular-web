'use strict';

angular.module('myApp.navigation', [])

.controller('NavigationCtrl', ['$scope', 'Auth', '$state', '$mdSidenav', function($scope, Auth, $state, $mdSidenav) {
  $scope.logout = function () {
    Auth.$unauth();
    $state.go("homepage");
  };

  $scope.toggleSidenav = function() {
    $mdSidenav('left').toggle();
  };
}]);

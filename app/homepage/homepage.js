'use strict';

angular.module('myApp.homepage', [])

.controller('HomepageCtrl', ['$scope', 'Auth', '$mdDialog', '$state', function($scope, Auth, $mdDialog, $state) {

  $scope.register = function(ev) {
    $mdDialog.show({
      templateUrl: 'dialog-register/dialog-register.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      controller: 'DialogRegisterCtrl',
    });
  };

  $scope.login = function(ev) {
    $mdDialog.show({
      templateUrl: 'dialog-login/dialog-login.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      controller: 'DialogLoginCtrl',
    });
  };

  $scope.imagePath = 'img/homepage.png';

  Auth.$onAuth(function(authData) {
    $scope.authData = authData;
    $state.go("navigation.dashboard");
    //console.log(authData);
  });

  $scope.logout = function () {
    Auth.$unauth();
  };


}]);

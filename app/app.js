'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ui.router',
  'ngMaterial',
  'myApp.firebase-auth',
  'myApp.firebase-debt-array',
  'myApp.firebase-credit-array',
  'myApp.homepage',
  'myApp.login',
  'myApp.register',
  'myApp.navigation',
  'myApp.dashboard',
  'myApp.addDebt',
  'myApp.addCredit',
  //'myApp.viewDebt',
  'myApp.version',
  'ngMaterial',
  'ngMdIcons',
  'firebase'
])

.run(["$rootScope", "$state", function($rootScope, $state) {
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
  // We can catch the error thrown when the $requireAuth promise is rejected
  // and redirect the user back to the home page
  if (error === "AUTH_REQUIRED") {
    $state.go("homepage");
  }
  });
}])

.config(['$stateProvider', '$routeProvider', '$locationProvider', function($stateProvider, $routeProvider, $locationProvider) {
  $routeProvider.otherwise({redirectTo: ''});
  //$locationProvider.html5Mode(true);
  $stateProvider
  .state('homepage', {
    url: '',
    templateUrl: 'homepage/homepage.html',
    controller: 'HomepageCtrl',
    resolve: {
      "currentAuth": ["Auth", function(Auth) {
        // $waitForAuth returns a promise so the resolve waits for it to complete
        return Auth.$waitForAuth();
      }]
    }
  })
  .state('navigation', {
    templateUrl: 'navigation/navigation.html',
    controller: 'NavigationCtrl'
  })
  .state('navigation.dashboard', {
    url: '/dashboard',
    templateUrl: 'dashboard/dashboard.html',
    controller: 'DashboardCtrl',
    resolve: {
      // controller will not be loaded until $requireAuth resolves
      // Auth refers to our $firebaseAuth wrapper in the example above
      "currentAuth": ["Auth", function(Auth) {
        // $requireAuth returns a promise so the resolve waits for it to complete
        // If the promise is rejected, it will throw a $stateChangeError (see above)
        return Auth.$requireAuth();
      }]
    }
  });
}])

.config(['$mdThemingProvider', function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    //.accentPalette('light-green')
    .warnPalette('cyan');
}]);

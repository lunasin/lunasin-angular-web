'use strict';

angular.module('myApp.addCredit', ['ngRoute'])

.controller('DialogAddCreditCtrl', ['$scope', '$firebaseArray', '$mdDialog', 'CreditArray', 'currentAuth', function($scope, $firebaseArray, $mdDialog, CreditArray, currentAuth) {

  //var ref = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/credits');

  //var lists = $firebaseArray(ref);

  var lists = CreditArray.setCurrentAuth(currentAuth);

  $scope.addCredit = function() {
    var input_list = {
      amount: $scope.credit.amount,
      debtor: $scope.credit.debtor,
      deadline: $scope.credit.deadline,
      note: $scope.credit.note,
    };
    console.log(input_list);
    lists.$add(input_list);
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

}]);

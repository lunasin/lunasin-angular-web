'use strict';

angular.module('myApp.addDebt', ['ngRoute'])

.controller('DialogAddDebtCtrl', ['$scope', '$firebaseArray', '$mdDialog', 'DebtArray', 'currentAuth', function($scope, $firebaseArray, $mdDialog, DebtArray, currentAuth) {

  //var ref = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/debts');

  //var lists = $firebaseArray(ref);

  var lists = DebtArray.setCurrentAuth(currentAuth);

  $scope.addDebt = function() {
    var input_list = {
      amount: $scope.debt.amount,
      creditor: $scope.debt.creditor,
      deadline: $scope.debt.deadline,
      note: $scope.debt.note,
    };
    console.log(input_list);
    lists.$add(input_list);
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

}]);

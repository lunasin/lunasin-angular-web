'use strict';

angular.module('myApp.dashboard', [])

.controller('DashboardCtrl', ['$scope', '$mdDialog', 'currentAuth', 'CreditArray', 'DebtArray', '$firebaseArray', function($scope, $mdDialog, currentAuth, CreditArray, DebtArray, $firebaseArray) {

  console.log(currentAuth.uid);

  /*var refCredits = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/credits');
  var refDebts = new Firebase('https://lunasin-web.firebaseio.com/users/' + currentAuth.uid + '/debts');

  $scope.creditLists = $firebaseArray(refCredits);
  $scope.debtLists = $firebaseArray(refDebts);*/

  $scope.creditLists = CreditArray.setCurrentAuth(currentAuth);
  $scope.debtLists = DebtArray.setCurrentAuth(currentAuth);

  var sum = function (lists, property) {
    if (lists == null) {
      console.log("noooooooooool");
      return 0;
    }
    //console.log(lists);
    return lists.reduce(function (a, b) {
      console.log(a);
      console.log(b[property]);
      return parseInt(b[property]) == null ? a : a + parseInt(b[property]);
    }, 0);
  };

  var sumDebts = function() {
    $scope.totalHutang = sum($scope.debtLists, "amount");
  }

  $scope.debtLists.$loaded()
    .then(sumDebts);
  //console.log(cobalist);

  $scope.dialogAddDebt = function(ev) {
    $mdDialog.show({
      templateUrl: 'dialog-add-debt/dialog-add-debt.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      //bindToController: true,
      //controlleAs: 'ctrl',
      controller: 'DialogAddDebtCtrl',
      resolve: {
        "currentAuth": ["Auth", function(Auth) {
          // $requireAuth returns a promise so the resolve waits for it to complete
          // If the promise is rejected, it will throw a $stateChangeError (see above)
          return Auth.$requireAuth();
        }]
      }
    }).then(function() {
        sumDebts();
    });
  };

  $scope.dialogAddCredit = function(ev) {
    $mdDialog.show({
      templateUrl: 'dialog-add-credit/dialog-add-credit.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      //bindToController: true,
      //controlleAs: 'ctrl',
      controller: 'DialogAddCreditCtrl',
      resolve: {
        "currentAuth": ["Auth", function(Auth) {
          // $requireAuth returns a promise so the resolve waits for it to complete
          // If the promise is rejected, it will throw a $stateChangeError (see above)
          return Auth.$requireAuth();
        }]
      }
    });
  };
}]);
